<?php

namespace SareApi\Action;

use SareApi\Sare;
use SareApi\Request\Send;
use SareApi\Exception\Exception;

class UpdateEmail extends Sare {

    public function execute($email, $name) {
        $response = $this->add($email, null, ['s_no_send' => 1]);
        if (!($response instanceof \SareApi\Response\Response)) {
            throw new Exception('Nie można pobrać mkey dla podanego adresu email', -200);
        }

        $params = $this->getParams($response->key(), $name);
        $url = $this->getUrl($params);
        $sObj = new Send($url, 'remove');
        return $sObj->execute();
    }

    private function getParams($mkey, $name = null) {
        $params = [
            's_uid=' . urlencode($this->s_uid),
            's_mkey=' . $mkey,
            's_encoded=' . $this->s_encoded,
            's_ip=' . urlencode($_SERVER['REMOTE_ADDR']),
            's_rv=1',
            's_no_send=' . $this->s_no_send,
        ];

        if (null !== $name) {
            $params[] = 's_name=' . urlencode($name);
        }

        if (is_array($this->s_group)) {
            foreach ($this->s_group as $group) {
                $params[] = 's_group_' . $group . '=1';
            }
        } elseif ($this->s_group) {
            $params[] = 's_group_' . $this->s_group . '=1';
        }

        if (is_array($this->s_groupoff)) {
            foreach ($this->s_groupoff as $group) {
                $params[] = 's_groupoff_' . $group . '=1';
            }
        } elseif ($this->s_groupoff) {
            $params[] = 's_groupoff_' . $this->s_groupoff . '=1';
        }
        return $params;
    }

    private function getUrl(array $params) {
        $requestString = implode('&', $params);
        return parent::HOST . '/upd.php?' . $requestString;
    }

}
