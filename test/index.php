<?php

require_once 'autoload.php';

$avGroups = [
    1, 2, 3
];

$config = [
    's_uid' => 5202,
    's_key' => '*76E51D54667EB98C7EB48A77A1EE6EB7C6064A09',
];

$sare = new \SareApi\Sare($config);

function title($t) {
    echo '<h1>' . $t . '</h1>' . PHP_EOL;
}

title('dodanie');
$r = $sare->add('marcin.matras@nomonday.pl', 'Test', ['s_group' => [1, 2]]);
echo $r->code();

title('sprawdzenie');
$r = $sare->check('marcin.matras@nomonday.pl');
echo $r->code();

title('usunięcie grupy 2 i 3');
$r = $sare->update('marcin.matras@nomonday.pl', null, ['s_groupoff' => [2, 3]]);
echo $r->code();

title('dodanie grupy 3');
$r = $sare->update('marcin.matras@nomonday.pl', null, ['s_group' => [3]]);
echo $r->code();

title('usunięcie adresu');
$r = $sare->remove('marcin.matras@nomonday.pl');
echo $r->code();

